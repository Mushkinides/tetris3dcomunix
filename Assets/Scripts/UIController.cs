﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;
    public Text scoreText;
    public Text layersText;
    public Text levelText;

    public GameObject gameOverScreen;

    private void Awake() 
    {
        instance = this;
    }
    private void Start() 
    {
        gameOverScreen.SetActive(false);
    }

    /*
        * Updates the UI for the score and layers text fields.
    */
    public void UpdateScoreUI(int score, int  layers)
    {
        scoreText.text = "Score: " + score.ToString();
        layersText.text = "Layers: " + layers.ToString();
    }

    /*
        * Updates the UI for the (difficulty) level text field.
    */
    public void UpdateLevelUI(int level)
    {
        levelText.text = "Level: " + level.ToString();
    }

    /*
        * Shows the Game-Over screen.
    */
    public void SetGameOverScreen()
    {
        gameOverScreen.SetActive(true);
    }
}
