﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    Transform camTarget;
    Transform camRotarionTarget;
    Vector3 lastMousePosition;
    float camSensitivity = 0.25f;

    private void Awake()
    {
        camRotarionTarget = transform.parent;
        camTarget = camRotarionTarget.transform.parent;
    }

    private void Update()
    {
        transform.LookAt(camTarget);
        if(Input.GetMouseButtonDown(0)) lastMousePosition = Input.mousePosition;
        CameraMovement();
    }

    /*
        * Lets the player control the camera movement around the
        * play board and clamps the angle so there won't be any
        * camera-shake glitchs.
    */
    private void CameraMovement()
    {
        if(Input.GetMouseButton(0))
        {
            Vector3 delta = Input.mousePosition - lastMousePosition;
            float angleY = -delta.y * camSensitivity;
            float angleX = delta.x * camSensitivity;

            // Left and right camera movement
            camTarget.RotateAround(camTarget.position, Vector3.up, angleX);
            lastMousePosition = Input.mousePosition;

            // Up and down camera movement
            Vector3 angles = camRotarionTarget.transform.eulerAngles;
            angles.x += angleY;
            angles.x = CamAngleClamp(angles.x, -88f, 88f);
            camRotarionTarget.transform.eulerAngles = angles;
        }

        float CamAngleClamp(float angle, float from, float to) {
            {
                if (angle < 0) angle = 360 + angle;
                if (angle > 180f) return Mathf.Max(angle, 360 + from);
                return Mathf.Min(angle, to);
            }
        }
    }

}
