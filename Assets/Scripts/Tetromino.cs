﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour
{
    public GameObject cam;
    public GameObject playBoard;
    private float prevTime;
    private float fallTime;
    private int initDiff;
    private int currDiff;
    private int diffPeriod;  
    private Vector3 camF;
    private Vector3 camR;  
    void Start()
    {
        cam = GameObject.Find("Main Camera");
        playBoard = GameObject.Find("PlayBoard");
        currDiff = PlayBoard.instance.currDiff;
        initDiff = playBoard.GetComponent<PlayBoard>().initialDiff;
        fallTime = (float)initDiff / (1 + 0.5f * (float)currDiff);
        if(!IsValidMove())
        {
            GameManager.instance.SetIsGameOver();
        }

    }

    void Update()
    {
        if(Time.time - prevTime > fallTime)
        {
            transform.position += Vector3.down;
            if(!IsValidMove()) 
            {
                transform.position += Vector3.up;
                PlayBoard.instance.RemoveLayer();
                enabled = false;
                if(!GameManager.instance.IsGameOver())
                {
                    PlayBoard.instance.SpawnNewTetromino();
                }
            }
            else 
            {
                PlayBoard.instance.UpdateBoard(this);
            }
            
            prevTime = Time.time;
        }

        // INPUT RELATIVE TO CAMERA ANGLE LOGIC
        Vector3 camF = cam.transform.forward;
        Vector3 camR = cam.transform.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;
        if(Mathf.Abs(camF.x) < Mathf.Abs(camF.z))
        {
            if(camF.z > 0) 
            {
                camF.z = 1;
            }
            else
            {
                camF.z = -1;
            }
            camF.x = 0;
        }
        else
        {
            camF.z = 0;
            if(camF.x > 0)
            {
                camF.x = 1;
            }
            else
            {
                camF.x = -1;
            }
        }
        if(Mathf.Abs(camR.x) < Mathf.Abs(camR.z))
        {
            if(camR.z > 0)
            {
                camR.z = 1;
            }
            else 
            {
                camR.z = -1;
            }
            camR.x = 0;
        }
        else
        {
            camR.z = 0;
            if(camR.x > 0) 
            {
                camR.x = 1;
            }
            else 
            {
                camR.x = -1;
            }
        }
        // PLAYER INPUT CONTROL
        if(Input.GetKeyDown(KeyCode.W)) 
        {
            SetDirection(camF);
        }
        if(Input.GetKeyDown(KeyCode.S)) 
        {
            SetDirection(-camF);
        }
        if(Input.GetKeyDown(KeyCode.D)) 
        {
            SetDirection(camR);
        }
        if(Input.GetKeyDown(KeyCode.A)) 
        {
            SetDirection(-camR);
        }
        if(Input.GetKeyDown(KeyCode.Z)) 
        {
            SetRotation(camR * 90);
        }
        if(Input.GetKeyDown(KeyCode.X)) 
        {
            SetRotation(new Vector3(0,90,0));
        }
        if(Input.GetKeyDown(KeyCode.C)) 
        {
            SetRotation(camF * 90);
        }
        if(Input.GetKeyDown(KeyCode.Space)) 
        {
            SetSpeed();
        }
        if(Input.GetKeyUp(KeyCode.Space)) 
        {
            ResetSpeed();
        }
    }

    /*
        * This function is incharge of the movement
        * of the tetromino. Checking if the movement
        * is valid and updating the board with the relevant
        * data.
    */
    public void SetDirection(Vector3 direction)
    {
        transform.position += direction;

        if(!IsValidMove()) 
        {
            transform.position -= direction;
        }
        else
        { 
            PlayBoard.instance.UpdateBoard(this);
        }
    }

    /*
        * This function is incharge of the rotation
        * of the tetromino. Checking if the rotation
        * is valid and updating the board with the relevant
        * data. 
    */
    public void SetRotation(Vector3 rotation)
    {
        transform.Rotate(rotation, Space.World);

        if(!IsValidMove()) 
        {
            transform.Rotate(-rotation, Space.World);
        }
        else
        {
            PlayBoard.instance.UpdateBoard(this);
        }
    }

    /*
        * When the player wants to speed up the tetromino
        * falling down then this functions sets its speed
        * to a faster one.
    */
    public void SetSpeed()
    {
        fallTime = 0.1f;
    }

    /*
        * When the player releases the speed-up button
        * then the relevant speed should be re-assigned
        * to the tetromino.
    */
    private void ResetSpeed()
    {
        fallTime = (float)initDiff / (1 + 0.5f * (float)currDiff);
        // if(fallTime < 0.5f) fallTime = 0.5f;
    }

    /*
        * THE MOST IMPORTANT FUNCITON FOR THE GAME :P
        * This checks if any movement/rotation/falling is actually
        * "legal" in terms of the game.
    */
    bool IsValidMove()
    {
        foreach(Transform child in transform)
        {
            Vector3 pos = PlayBoard.instance.Round(child.position);
            if(!PlayBoard.instance.IsInsidePlayBoard(pos))
            {
                return false;
            }
        }
        foreach (Transform child in transform)
        {
            Vector3 pos = PlayBoard.instance.Round(child.position);
            Transform t = PlayBoard.instance.GetTransformOnBoardPos(pos);
            if(t && t.parent != transform) return false;
        }
        return true;
    }
}
