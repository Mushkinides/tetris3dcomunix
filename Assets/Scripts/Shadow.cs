﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    GameObject parent;
    Tetromino parentTetromino;

    
    void Start()
    {
        StartCoroutine(RepositionShadow());
    }

    /*
        * We set the parent of the shadow to the active
        * falling tetromino to mimic the movements and 
        * rotation.
    */
    public void SetParent(GameObject _parent)
    {
        parent = _parent;
        parentTetromino = parent.GetComponent<Tetromino>();
    }

    /*
        * As long as the Tetromino is active so is
        * its shadow. Then we destroy it to avoid
        * multiple shadows on the play board.
        * This method also is a bit more efficient
        * rather than putting this in Update().
    */
    IEnumerator RepositionShadow()
    {
        while(parentTetromino.enabled)
        {
            ShadowPosition();
            GroundShadow();


            yield return new WaitForSeconds(0.1f);
        }
        Destroy(gameObject);
        yield return null;
    }

    /*
        * Setting the position and rotation of the shadow.
    */
    private void ShadowPosition()
    {
        transform.position = parent.transform.position;
        transform.rotation = parent.transform.rotation;
    }

    /*
        * Ground the shadow to the lowest place that it
        * can considering the play board ground and other
        * tetrominos that are blocking it's path.
    */
    private void GroundShadow()
    {
        while(IsValidMove())
        {
            transform.position += Vector3.down;
        }
        if(!IsValidMove()) transform.position += Vector3.up;

    }

    /*
        * Checks if the shadow can be in a certain
        * position after either moving or rotating the 
        * tetromino.
    */
    private bool IsValidMove()
    {
        foreach(Transform child in transform)
        {
            Vector3 pos = PlayBoard.instance.Round(child.position);
            if(!PlayBoard.instance.IsInsidePlayBoard(pos))
            {
                return false;
            }
        }
        foreach (Transform child in transform)
        {
            Vector3 pos = PlayBoard.instance.Round(child.position);
            Transform t = PlayBoard.instance.GetTransformOnBoardPos(pos);
            if(t && t.parent == parent.transform) return true;
            
            
            if(t && t.parent != transform) return false;
        }
        return true;
    }
}
