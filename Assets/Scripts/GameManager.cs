﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private int score;
    private int level;
    private int layersCompleted;
    private bool isGameOver;
    

    enum ScorePerLayer
    {
        ONE_LAYER = 100,
        TWO_LAYERS = 200,
        THREE_LAYERS = 400,
        FOUR_LAYERS = 800
    };

    private void Awake() 
    {
        instance = this;    
    }
    private void Start() 
    {
        UpdateScore(score);
        UpdateLevel(level);    
    }

    /*
        * When a player clears a layer we want to give him
        * points based on the number of layers he has cleared
        * in "one turn". That's why we calculate it here.
    */
    public void UpdateScore(int value)
    {
        switch (value)
        {
            case 1:
                score += (int)ScorePerLayer.ONE_LAYER;
                break;
            case 2:
                score += (int)ScorePerLayer.TWO_LAYERS;
                break;
            case 3:
                score += (int)ScorePerLayer.THREE_LAYERS;
                break;
            case 4:
                score += (int)ScorePerLayer.FOUR_LAYERS;
                break;
        }
        UIController.instance.UpdateScoreUI(score, layersCompleted);
    }

    /*
        * Updates the variable to keep track on how many
        * layers has the player cleared so far.
    */
    public void UpdateLayersCompleted(int value)
    {
        layersCompleted += value;
    }

    /*
        * Updates the difficulty level (fall speed of the tetromino)
        * to keep track on what difficulty is the player.
    */
    public void UpdateLevel(int value)
    {
        level = value + 1;
        UIController.instance.UpdateLevelUI(level);
    }

    /*
        * Gets the value of 'isGameOver' to know when
        * to stop the game and present a game-over screen.
    */
    public bool IsGameOver()
    {
        return isGameOver;
    }

    /*
        * Sets the variable 'isGameOver' to true
        * and starts a chain of events that show the 
        * game-over screen.
    */
    public void SetIsGameOver()
    {
        isGameOver = true;
        UIController.instance.SetGameOverScreen();
    }
}
