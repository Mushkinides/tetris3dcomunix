﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextRotate : MonoBehaviour
{
    public float speed;
    public GameObject NextCam;
    
    private void Awake() 
    {
        NextCam = GameObject.Find("Next Camera");  
    }
    void Update()
    {
        transform.RotateAround(transform.position, NextCam.transform.up, speed * Time.deltaTime);
    }
}
