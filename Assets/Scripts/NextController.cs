﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextController : MonoBehaviour
{
    public static NextController instance;
    public GameObject[] nextTetrominos;
    GameObject currentActive;

    private void Awake() 
    {
        instance = this;
    }

    /*
        * This function is incharge of showing the
        * next tetromino to spawn in the game after
        * the current falling tetromino is placed.
    */
    public void ShowNext(int index)
    {
        Destroy(currentActive);
        currentActive = Instantiate(nextTetrominos[index], transform.position, transform.rotation) as GameObject;
        currentActive.transform.parent = transform;
    }
}
