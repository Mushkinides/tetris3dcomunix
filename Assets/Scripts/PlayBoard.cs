﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBoard : MonoBehaviour 
{
    public static PlayBoard instance;
    public Transform[,,] theBoard;

    [Header("Difficulty Settings")]
    public int initialDiff;
    public int currDiff;
    public int diffPeriod;
    private float diffCounter;
    private int randomIndex;
    private int spawnIndex;

    [Header("Tetrominos")]
    public GameObject[] tetrominoArray;
    public GameObject[] shadowArray;
    public float[] spawnOdds;
    
    [Header ("PlayBoard Visual")]
    public int boardSizeX;
    public int boardSizeY; 
    public int boardSizeZ;
    public GameObject groundPlane;
    public GameObject frontPlane, backPlane, rightPlane, leftPlane;

   
    private void Awake() 
    {
        instance = this;
    }
    private void Start() 
    {
        theBoard = new Transform[boardSizeX, boardSizeY, boardSizeZ];
        LegalOdds();
        RewriteOdds();
        WhoIsNext();
        SpawnNewTetromino();
    }
    
    /*
        * Checks if the odds entered in the Unity interface sum up to 100 (100%)
        * and if it doesn't then a correction is made to spawn each tetromino 
        * at an eqal chance.
    */
    private void LegalOdds()
    {
        float sum = 0;
        for (int i = 0; i < spawnOdds.Length; i++)
        {
            sum += spawnOdds[i];
        }

        if(sum != 100)
        {
            for (int i = 0; i < spawnOdds.Length; i++)
            {
                spawnOdds[i] = (100/(float)spawnOdds.Length);
            }
        }
    }

    /*
        * The function rewrites the odds as an assending
        * array to simplify the calculations of the chance
        * to spawn a certain tetromino.
    */
    private void RewriteOdds()
    {
        for (int i = 0; i < spawnOdds.Length; i++)
        {
            if(i!=0) spawnOdds[i] += spawnOdds[i-1];
        }
    }
    private void Update() 
    {
        // DIFFICULTY LOGIC
        diffCounter += Time.deltaTime;

        if (diffCounter >= diffPeriod)
        {
            currDiff += 1; 
            diffCounter = 0;
            GameManager.instance.UpdateLevel(currDiff);
        }    
    }

    /*
        * This function rounds the vectors it is given
        * because all of the game movements are done in
        * a grid.
    */
    public Vector3 Round(Vector3 vector) 
    {
        return new Vector3(Mathf.RoundToInt(vector.x), Mathf.RoundToInt(vector.y), Mathf.RoundToInt(vector.z));
    }

    /*
        * This bool function checks to see if a tetromino (or part of it) 
        * would leave the play board due to either turning it or "falling"
        * through the ground.
    */
    public bool IsInsidePlayBoard(Vector3 pos) 
    {
        return ((int)pos.x >= 0 && (int)pos.x < boardSizeX && (int)pos.z >= 0 && (int)pos.z < boardSizeZ && (int)pos.y >=0);
    }

    /*
        * The function updates the board with the transform
        * of all the childs (cubes) of their parent GameObject
        * (The Tetromino itself). This is to keep track which 
        * cells are currently occupied with a block and which
        * are empty.
    */
    public void UpdateBoard(Tetromino tetromino)
    {
        for (int x = 0; x < boardSizeX; x++)
        {
            for (int z = 0; z < boardSizeZ; z++)
            {
                for (int y = 0; y < boardSizeY; y++)
                {
                    if(theBoard[x,y,z] && theBoard[x,y,z].parent == tetromino.transform)
                    {
                        theBoard[x,y,z] = null;
                    }
                }
            }
        }
        foreach (Transform child in tetromino.transform)
        {
            Vector3 pos = Round(child.position);
            if(pos.y < boardSizeY)
            {
                theBoard[(int)pos.x, (int)pos.y, (int)pos.z] = child;
            }
        }
    }

     /*
        * We check to see that we won't intersect 2 tetrominos together
        * or go through the ground if we try to make some kind of movement (move, go down or rotate).
     */
    public Transform GetTransformOnBoardPos(Vector3 pos)
    {
        if(pos.y > boardSizeY-1)
        {
            return null;
        }
        else 
        {
            return theBoard[(int)pos.x, (int)pos.y, (int)pos.z];
        }
    }

    /*
        * Spawns a new Tetromino. This is called when we need a new
        * Tetromino which is when we either start the game or just
        * finished placing the previous tetromino.
        * The function also spawns the "shadow" effect on the ground
        * and the next tetromino in the preview UI.
    */
   public void SpawnNewTetromino()
    {
       Vector3 spawnPoint = new Vector3((int)(transform.position.x + (float)boardSizeX / 2), (int)transform.position.y + boardSizeY, (int)(transform.position.z + (float)boardSizeZ / 2));
       GameObject newTetromino = Instantiate(tetrominoArray[spawnIndex], spawnPoint, Quaternion.identity) as GameObject;
       GameObject newShadow = Instantiate(shadowArray[spawnIndex], spawnPoint, Quaternion.identity) as GameObject;
        newShadow.GetComponent<Shadow>().SetParent(newTetromino);
        WhoIsNext();
        NextController.instance.ShowNext(spawnIndex);
    }

    /*
        * We want to generate randomly the next tetromino
        * and know what type it is to present it in the
        * UI fo the player. 
    */
    public void WhoIsNext()
    {
        randomIndex = Random.Range(0, 101);
        spawnIndex = 0;
        bool spawnIndexSet = false;
        for (int i = 0; i < spawnOdds.Length; i++)
        {
            if(randomIndex < spawnOdds[i] && !spawnIndexSet)
            {
                spawnIndex = i;
                spawnIndexSet = true;
            } 
        }
    } 

    /*
        * The function is incharge of the whole layer/score
        * logic. When the player fills a layer he should get points,
        * the full layer should be removed and all the tetromino parts
        * sitting on top of that layer should fall down by the amount of
        * layers removed.
    */
    public void RemoveLayer()
    {
        int layersRemoved = 0;
        for (int y = boardSizeY - 1; y >= 0; y--)
        {
            if(IsLayerFull(y))
            {
                layersRemoved++;
                RemoveLayerAt(y);
                MoveAllLayersDown(y);
            }
        }
        if(layersRemoved > 0)
        {
            GameManager.instance.UpdateLayersCompleted(layersRemoved);
            GameManager.instance.UpdateScore(layersRemoved);
        }
    }

    /*
        * Checks to see if the layer is full.
    */
    private bool IsLayerFull(int y)
    {
       for (int x = 0; x < boardSizeX; x++)
       {
           for (int z = 0; z < boardSizeZ; z++)
           {
               if(theBoard[x,y,z] == null) return false;
           }
       }
       return true;
    }

    /*
        * Removes a spesific layer at height y.
    */
    private void RemoveLayerAt(int y)
    {
        for (int x = 0; x < boardSizeX; x++)
        {
            for (int z = 0; z < boardSizeZ; z++)
            {
                Destroy(theBoard[x,y,z].gameObject);
                theBoard[x,y,z] = null;
            }
        }
    }

    /*
        * Moves all the layers down. This is because there
        * might be tetromino parts on top of the removed layers.
    */
    private void MoveAllLayersDown(int y)
    {
        for (int i = y; i < boardSizeY; i++)
        {
            MoveSingleLayerDown(i);
        }
    }

    /*
        * Moves a single layer down.
    */
    private void MoveSingleLayerDown(int y)
    {
        for (int x = 0; x < boardSizeX; x++)
        {
            for (int z = 0; z < boardSizeZ; z++)
            {
                if(theBoard[x,y,z])
                {
                    theBoard[x ,y - 1, z] = theBoard[x,y,z];
                    theBoard[x,y,z] = null;
                    theBoard[x, y - 1, z].position += Vector3.down;
                }
            }
        }
    }

    /*
        * Redraws the play board and repositions camera
        * based on the values the designer plugged into
        * the Unity Editor.
    */
    private void OnDrawGizmos() 
    {
        // Resizing, positioning the Ground and retile material
        if(groundPlane) 
        {
            Vector3 boardScale = new Vector3((float)boardSizeX/10,1,(float)boardSizeZ/10);
            groundPlane.transform.localScale = boardScale;
            groundPlane.transform.position = new Vector3(transform.position.x + (float)boardSizeX / 2, transform.position.y, transform.position.z + (float)boardSizeZ / 2);
            groundPlane.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(boardSizeX, boardSizeZ);
        }  

        // Resizing, positioning the Front & Back and retile material
        if(frontPlane) 
        {
            Vector3 boardScale = new Vector3((float)boardSizeX/10,1,(float)boardSizeY/10);
            frontPlane.transform.localScale = boardScale;
            frontPlane.transform.position = new Vector3(transform.position.x + (float)boardSizeX / 2, transform.position.y + (float)boardSizeY / 2, transform.position.z + boardSizeZ);
            frontPlane.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(boardSizeX, boardSizeY);
        }  
        if(backPlane) 
        {
            Vector3 boardScale = new Vector3((float)boardSizeX/10,1,(float)boardSizeY/10);
            backPlane.transform.localScale = boardScale;
            backPlane.transform.position = new Vector3(transform.position.x + (float)boardSizeX / 2, transform.position.y + (float)boardSizeY / 2, transform.position.z);
        } 

        // Resizing, positioning the Left & Right and retile material
        if(leftPlane) 
        {
            Vector3 boardScale = new Vector3((float)boardSizeZ/10,1,(float)boardSizeY/10);
            leftPlane.transform.localScale = boardScale;
            leftPlane.transform.position = new Vector3(transform.position.x, transform.position.y + (float)boardSizeY / 2, transform.position.z + (float)boardSizeZ / 2);
            leftPlane.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(boardSizeZ, boardSizeY);
        }  
        if(rightPlane) 
        {
            Vector3 boardScale = new Vector3((float)boardSizeZ/10,1,(float)boardSizeY/10);
            rightPlane.transform.localScale = boardScale;
            rightPlane.transform.position = new Vector3(transform.position.x + boardSizeX, transform.position.y + (float)boardSizeY / 2, transform.position.z + (float)boardSizeZ / 2);
        } 
        // Calculating camera new positions
        GameObject camTarget = GameObject.Find("CamTarget");
        camTarget.transform.position = new Vector3(((float)boardSizeX/2 - 0.5f), ((float)boardSizeY/2 - 0.5f), ((float)boardSizeZ/2 - 0.5f));

    }
}
