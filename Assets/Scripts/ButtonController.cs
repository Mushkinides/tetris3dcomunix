﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    /*
        * When the player presses the "Play"
        * button we want to move him to the game
        * scene, and when he loses and the game-over
        * screen is presented, the button there retunrs
        *him to the title screen.
    */
    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
